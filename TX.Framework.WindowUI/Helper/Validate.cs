﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TX.Framework.WindowUI.Helper
{
    public class Validate
    {
        public static bool IsEmpty(object data)
        {
            return data == null;
        }

        public static bool IsEmpty(string data)
        {
            return data == null || string.IsNullOrWhiteSpace(data);
        }

        public static bool IsEmpty(Guid? data)
        {
            return data == null || data == Guid.Empty;
        }

        public static bool IsEmpty(Guid data)
        {
            return data == Guid.Empty;
        }

        public static bool IsEmpty(int? data)
        {
            return data == null || data == 0;
        }

        public static bool IsEmpty<T>(T data)
        {
            bool result;
            if (data == null)
            {
                result = true;
            }
            else if (data is string && string.IsNullOrWhiteSpace(data.ToString()))
            {
                result = true;
            }
            else
            {
                if (data is Guid)
                {
                    if (ConvertHelper.ToGuid(data) == Guid.Empty)
                    {
                        return true;
                    }
                }
                result = false;
            }
            return result;
        }
    }
}
