﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TX.Framework.WindowUI.Helper
{
    public static class ConvertHelper
    {
        public static byte ToByte(object data)
        {
            byte result;
            if (data == null)
            {
                result = 0;
            }
            else
            {
                try
                {
                    result = Convert.ToByte(data);
                }
                catch
                {
                    result = 0;
                }
            }
            return result;
        }

        public static int ToInt(object data)
        {
            int result;
            if (data == null)
            {
                result = 0;
            }
            else
            {
                try
                {
                    result = Convert.ToInt32(data);
                }
                catch
                {
                    result = 0;
                }
            }
            return result;
        }

        public static string ToInt(string data, int from, int to)
        {
            string result;
            if (data == null)
            {
                result = string.Empty;
            }
            else
            {
                try
                {
                    int value = Convert.ToInt32(data.ToString(), from);
                    string text = System.Convert.ToString(value, to);
                    if (to == 2)
                    {
                        switch (text.Length)
                        {
                            case 3:
                                text = "00000" + text;
                                break;
                            case 4:
                                text = "0000" + text;
                                break;
                            case 5:
                                text = "000" + text;
                                break;
                            case 6:
                                text = "00" + text;
                                break;
                            case 7:
                                text = "0" + text;
                                break;
                        }
                    }
                    result = text;
                }
                catch
                {
                    result = string.Empty;
                }
            }
            return result;
        }

        // Token: 0x060002FD RID: 765 RVA: 0x0000B30C File Offset: 0x0000950C
        public static int ToInt32(object data)
        {
            return ToInt(data);
        }

        // Token: 0x060002FE RID: 766 RVA: 0x0000B324 File Offset: 0x00009524
        public static int ToInt32(object data, IFormatProvider format)
        {
            return Convert.ToInt32(data, format);
        }

        // Token: 0x060002FF RID: 767 RVA: 0x0000B340 File Offset: 0x00009540
        public static int ToInt32(string data, int fromBase)
        {
            return Convert.ToInt32(data, fromBase);
        }

        // Token: 0x06000300 RID: 768 RVA: 0x0000B35C File Offset: 0x0000955C
        public static int ToUInt16(string data)
        {
            return (int)Convert.ToUInt16(data);
        }

        // Token: 0x06000301 RID: 769 RVA: 0x0000B374 File Offset: 0x00009574
        public static int ToInt16(string data)
        {
            return (int)Convert.ToInt16(data);
        }

        // Token: 0x06000302 RID: 770 RVA: 0x0000B38C File Offset: 0x0000958C
        public static int? ToIntOrNull(object data)
        {
            int? result;
            if (data == null)
            {
                result = null;
            }
            else
            {
                try
                {
                    result = new int?(Convert.ToInt32(data));
                }
                catch
                {
                    result = null;
                }
            }
            return result;
        }

        // Token: 0x06000303 RID: 771 RVA: 0x0000B3E4 File Offset: 0x000095E4
        public static long ToLong(object data)
        {
            long result;
            if (data == null)
            {
                result = 0L;
            }
            else
            {
                try
                {
                    result = System.Convert.ToInt64(data);
                }
                catch
                {
                    result = 0L;
                }
            }
            return result;
        }

        // Token: 0x06000304 RID: 772 RVA: 0x0000B428 File Offset: 0x00009628
        public static long? ToLongOrNull(object data)
        {
            long? result;
            if (data == null)
            {
                result = null;
            }
            else
            {
                try
                {
                    result = new long?(System.Convert.ToInt64(data));
                }
                catch
                {
                    result = null;
                }
            }
            return result;
        }

        // Token: 0x06000305 RID: 773 RVA: 0x0000B480 File Offset: 0x00009680
        public static Guid ToGuid(object data)
        {
            Guid result;
            if (data == null)
            {
                result = Guid.Empty;
            }
            else
            {
                try
                {
                    result = new Guid(data.ToString());
                }
                catch
                {
                    result = Guid.Empty;
                }
            }
            return result;
        }

        // Token: 0x06000306 RID: 774 RVA: 0x0000B4D0 File Offset: 0x000096D0
        public static Guid? ToGuidOrNull(object data)
        {
            Guid? result;
            if (data == null)
            {
                result = null;
            }
            else
            {
                try
                {
                    result = new Guid?(new Guid(data.ToString()));
                }
                catch
                {
                    result = null;
                }
            }
            return result;
        }

        // Token: 0x06000307 RID: 775 RVA: 0x0000B52C File Offset: 0x0000972C
        public static DateTime ToDateTime(object data)
        {
            DateTime result;
            if (data == null)
            {
                result = DateTime.MinValue;
            }
            else
            {
                try
                {
                    result = Convert.ToDateTime(data);
                }
                catch
                {
                    result = DateTime.MinValue;
                }
            }
            return result;
        }

        // Token: 0x06000308 RID: 776 RVA: 0x0000B574 File Offset: 0x00009774
        public static DateTime? ToDateTimeOrNull(object data)
        {
            DateTime? result;
            if (data == null)
            {
                result = null;
            }
            else
            {
                try
                {
                    result = new DateTime?(Convert.ToDateTime(data));
                }
                catch
                {
                    result = null;
                }
            }
            return result;
        }

        // Token: 0x06000309 RID: 777 RVA: 0x0000B5CC File Offset: 0x000097CC
        public static string ToString(object data)
        {
            string result;
            if (data == null)
            {
                result = string.Empty;
            }
            else
            {
                try
                {
                    result = data.ToString().Trim();
                }
                catch
                {
                    result = string.Empty;
                }
            }
            return result;
        }

        public static bool ToBool(object data)
        {
            bool result;
            if (data == null)
            {
                result = false;
            }
            else
            {
                try
                {
                    string text = data.ToString().Trim().ToLower();
                    switch (text)
                    {
                        case "0":
                            return false;
                        case "1":
                            return true;
                        case "是":
                            return true;
                        case "否":
                            return false;
                        case "yes":
                            return true;
                        case "no":
                            return false;
                    }
                    result = System.Convert.ToBoolean(data);
                }
                catch
                {
                    result = false;
                }
            }
            return result;
        }

        // Token: 0x0600030B RID: 779 RVA: 0x0000B720 File Offset: 0x00009920
        public static bool ToBoolean(object data, IFormatProvider format)
        {
            return Convert.ToBoolean(data, format);
        }

        public static bool? ToBoolOrNull(object data)
        {
            bool? result;
            if (data == null)
            {
                result = null;
            }
            else
            {
                result = new bool?(ToBool(data));
            }
            return result;
        }

        public static float ToFloat(object data)
        {
            float result;
            if (data == null)
            {
                result = 0f;
            }
            else
            {
                try
                {
                    result = Convert.ToSingle(data);
                }
                catch
                {
                    result = 0f;
                }
            }
            return result;
        }

        public static float ToSingle(object data)
        {
            return ToFloat(data);
        }

        public static double ToDouble(object data)
        {
            double result;
            if (data == null)
            {
                result = 0.0;
            }
            else
            {
                try
                {
                    result = Convert.ToDouble(data);
                }
                catch
                {
                    result = 0.0;
                }
            }
            return result;
        }

        // Token: 0x06000310 RID: 784 RVA: 0x0000B824 File Offset: 0x00009A24
        public static double ToDouble(object data, int decimalDigits)
        {
            double result;
            if (data == null)
            {
                result = 0.0;
            }
            else
            {
                try
                {
                    double value = Convert.ToDouble(data);
                    result = Math.Round(value, decimalDigits);
                }
                catch
                {
                    result = 0.0;
                }
            }
            return result;
        }

        public static double ToDouble(object data, IFormatProvider format)
        {
            return Convert.ToDouble(data, format);
        }

        public static decimal ToDecimal(object data)
        {
            decimal result;
            if (data == null)
            {
                result = 0m;
            }
            else
            {
                try
                {
                    result = Convert.ToDecimal(data);
                }
                catch
                {
                    result = 0m;
                }
            }
            return result;
        }

        // Token: 0x06000313 RID: 787 RVA: 0x0000B8E4 File Offset: 0x00009AE4
        public static decimal ToDecimal(object data, int decimalDigits)
        {
            decimal result;
            if (data == null)
            {
                result = 0m;
            }
            else
            {
                try
                {
                    decimal d = Convert.ToDecimal(data);
                    result = Math.Round(d, decimalDigits);
                }
                catch
                {
                    result = 0m;
                }
            }
            return result;
        }

        // Token: 0x06000314 RID: 788 RVA: 0x0000B938 File Offset: 0x00009B38
        public static byte[] StringToBytes(string data)
        {
            return Encoding.GetEncoding("utf-8").GetBytes(data);
        }

        // Token: 0x06000315 RID: 789 RVA: 0x0000B95C File Offset: 0x00009B5C
        public static byte[] StringToBytes(string data, Encoding encoding)
        {
            return encoding.GetBytes(data);
        }

        // Token: 0x06000316 RID: 790 RVA: 0x0000B978 File Offset: 0x00009B78
        public static string BytesToString(byte[] data)
        {
            return Encoding.GetEncoding("utf-8").GetString(data);
        }

        // Token: 0x06000317 RID: 791 RVA: 0x0000B99C File Offset: 0x00009B9C
        public static string BytesToString(byte[] data, Encoding encoding)
        {
            return encoding.GetString(data);
        }

        public static string StreamToString(Stream stream)
        {
            return StreamToString(stream, Encoding.GetEncoding("utf-8"));
        }

        public static string StreamToString(Stream stream, Encoding encoding)
        {
            string result;
            if (stream == null)
            {
                result = string.Empty;
            }
            else
            {
                string text;
                using (StreamReader streamReader = new StreamReader(stream, encoding))
                {
                    text = streamReader.ReadToEnd();
                }
                result = text;
            }
            return result;
        }

        // Token: 0x0600031A RID: 794 RVA: 0x0000BA38 File Offset: 0x00009C38
        public static int BytesToInt(byte[] data)
        {
            int result;
            if (data.Length < 4)
            {
                result = 0;
            }
            else
            {
                byte[] array = new byte[4];
                Buffer.BlockCopy(data, 0, array, 0, 4);
                result = BitConverter.ToInt32(array, 0);
            }
            return result;
        }

        public static T ConvertTo<T>(object data)
        {
            T result;
            if (data == null)
            {
                result = default(T);
            }
            else
            {
                try
                {
                    result = (T)((object)Convert.ChangeType(data, typeof(T)));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        // Token: 0x0600031C RID: 796 RVA: 0x0000BAD0 File Offset: 0x00009CD0
        public static object ChangeType(object data, Type t)
        {
            string text = data.ToString();
            object result;
            if (t.Name.Equals("Guid"))
            {
                result = new Guid(text);
            }
            else
            {
                TypeCode typeCode = Type.GetTypeCode(t);
                TypeCode typeCode2 = typeCode;
                if (typeCode2 != TypeCode.Boolean)
                {
                    if (typeCode2 != TypeCode.Int32)
                    {
                        switch (typeCode2)
                        {
                            case TypeCode.Double:
                                {
                                    double num = 0.0;
                                    if (double.TryParse(text, out num))
                                    {
                                        object obj = num;
                                    }
                                    break;
                                }
                            case TypeCode.Decimal:
                                {
                                    decimal num2 = 0m;
                                    if (decimal.TryParse(text, out num2))
                                    {
                                        object obj = num2;
                                    }
                                    break;
                                }
                            case TypeCode.DateTime:
                                {
                                    DateTime dateTime;
                                    if (DateTime.TryParse(text, out dateTime))
                                    {
                                        object obj = dateTime;
                                    }
                                    break;
                                }
                        }
                    }
                    else
                    {
                        int num3 = 0;
                        if (int.TryParse(text, out num3))
                        {
                            object obj = num3;
                        }
                    }
                }
                else
                {
                    bool flag = false;
                    if (bool.TryParse(text, out flag))
                    {
                        object obj = flag;
                    }
                }
                if (data is IConvertible)
                {
                    result = Convert.ChangeType(data, t);
                }
                else
                {
                    result = data;
                }
            }
            return result;
        }

        public static int SafeValue(this int? obj)
        {
            return (obj != null) ? obj.Value : 0;
        }

        public static bool SafeValue(this bool? obj)
        {
            return obj != null && obj.Value;
        }

        public static DateTime SafeValue(this DateTime? obj)
        {
            return (obj != null) ? obj.Value : DateTime.MinValue;
        }

        public static decimal SafeValue(this decimal? obj)
        {
            return (obj != null) ? obj.Value : 0m;
        }
    }
}
