﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TX.Framework.WindowUI.Helper
{
    public class EnumHelper
    {
        public static T GetInstance<T>(string member)
        {
            T result;
            if (Validate.IsEmpty(member))
            {
                result = default(T);
            }
            else
            {
                Type type = Nullable.GetUnderlyingType(typeof(T));
                if (type == null)
                {
                    type = typeof(T);
                }
                result = (T)((object)System.Enum.Parse(type, member, true));
            }
            return result;
        }

        public static T GetInstance<T>(object member)
        {
            return EnumHelper.GetInstance<T>(ConvertHelper.ToString(member));
        }
        public static string GetMemberName<T>(object member)
        {
            string result;
            if (Validate.IsEmpty(member))
            {
                result = string.Empty;
            }
            else if (member.GetType().Equals(typeof(string)))
            {
                result = member.ToString();
            }
            else
            {
                Type type = Nullable.GetUnderlyingType(typeof(T));
                if (type == null)
                {
                    type = typeof(T);
                }
                Type underlyingType = System.Enum.GetUnderlyingType(type);
                member = ConvertHelper.ChangeType(member, underlyingType);
                result = System.Enum.GetName(type, member);
            }
            return result;
        }

        public static K GetMemberValue<T, K>(object member)
        {
            K result;
            if (Validate.IsEmpty(member))
            {
                result = default(K);
            }
            else if (member.GetType().Equals(typeof(int)) || member.GetType().Equals(typeof(char)))
            {
                result = (K)((object)member);
            }
            else
            {
                object obj = member;
                if (member.GetType().Equals(typeof(string)))
                {
                    obj = EnumHelper.GetInstance<T>(member);
                }
                if (obj == null)
                {
                    result = default(K);
                }
                else
                {
                    Type type = Nullable.GetUnderlyingType(typeof(T));
                    if (type == null)
                    {
                        type = typeof(T);
                    }
                    Type underlyingType = System.Enum.GetUnderlyingType(type);
                    result = (K)((object)ConvertHelper.ChangeType(obj, underlyingType));
                }
            }
            return result;
        }
        public static string GetMemberDescription<T>(object member)
        {
            string result;
            if (Validate.IsEmpty(member))
            {
                result = string.Empty;
            }
            else
            {
                Type type = Nullable.GetUnderlyingType(typeof(T));
                if (type == null)
                {
                    type = typeof(T);
                }
                if (!type.IsEnum)
                {
                    result = string.Empty;
                }
                else
                {
                    string memberName = EnumHelper.GetMemberName<T>(member);
                    if (Validate.IsEmpty(memberName))
                    {
                        result = string.Empty;
                    }
                    else
                    {
                        FieldInfo field = type.GetField(memberName);
                        if (Validate.IsEmpty<FieldInfo>(field))
                        {
                            result = string.Empty;
                        }
                        else
                        {
                            object obj = field.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault<object>();
                            if (Validate.IsEmpty(obj))
                            {
                                result = string.Empty;
                            }
                            else
                            {
                                DescriptionAttribute descriptionAttribute = (DescriptionAttribute)obj;
                                result = descriptionAttribute.Description;
                            }
                        }
                    }
                }
            }
            return result;
        }
    }
}
